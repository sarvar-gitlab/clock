function getTime(){
  let now = new Date();
  let h = now.getHours(), m = now.getMinutes(), s = now.getSeconds();

  document.getElementById("time-app").innerHTML = h+":"+m+":"+s;
  
  let gradusS = s*6, gradusM = m*6, gradusH = h*6;

  document.getElementById("arrow-s").style.transform = "translate(-50%)"+" rotateZ("+gradusS+"deg)";
  document.getElementById("arrow-m").style.transform = "translate(-50%)"+" rotateZ("+gradusM+"deg)";
  document.getElementById("arrow-h").style.transform = "translate(-50%)"+" rotateZ("+gradusH+"deg)";

  setTimeout(getTime,1000);
}

getTime();